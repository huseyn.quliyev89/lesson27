package az.ingress.enums;

public enum UserRole {
    ROLE_USER,
    ROLE_ADMIN
}
