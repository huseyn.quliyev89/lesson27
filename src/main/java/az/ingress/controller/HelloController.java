package az.ingress.controller;

import az.ingress.dto.JwtAuthResponse;
import az.ingress.dto.JwtRefreshTokenRequest;
import az.ingress.dto.LoginDto;
import az.ingress.dto.UserDto;
import az.ingress.service.AuthenticationService;
import az.ingress.service.UserService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
public class HelloController {
    private final UserService userService;
    private final AuthenticationService authenticationService;


    @GetMapping("/public")
    public String publicHello(){
        return "Hello Public";
    }

    @GetMapping("/user")
    public String userHello(){
        return "user Hello";
    }

    @GetMapping("/admin")
    public String adminHello(){
        return "admin Hello";
    }

    @PostMapping("/register/user")
    public UserDto registerUser(@RequestBody UserDto userDto) {
        return userService.registerUser(userDto);
    }

    @PostMapping("/register/admin")
    public UserDto registerAdmin(@RequestBody UserDto userDto) {
        return userService.registerAdmin(userDto);
    }

    @PostMapping("/login")
    public JwtAuthResponse login(@RequestBody LoginDto loginDto) {
        return authenticationService.login(loginDto);
    }

    @PostMapping("/token/refresh")
    public JwtAuthResponse refreshToken(HttpServletRequest request) {
        return authenticationService.refreshToken(request);
    }

}
