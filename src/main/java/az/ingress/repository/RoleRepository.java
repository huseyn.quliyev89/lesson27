package az.ingress.repository;

import az.ingress.model.Role;
import az.ingress.enums.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByName(UserRole userRole);
}
