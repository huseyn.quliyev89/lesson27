package az.ingress.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class JwtAuthResponse {
     String accessToken;
     String refreshToken;
     String tokenType ;
}
