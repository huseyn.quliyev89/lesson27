package az.ingress.service;

import az.ingress.dto.UserDto;
import az.ingress.enums.UserRole;
import az.ingress.model.User;

import java.util.Optional;

public interface UserService {
    UserDto save(UserDto userDto, UserRole roleName);

    User findByUsername(String username);
    UserDto registerUser(UserDto userDto);

    UserDto registerAdmin(UserDto userDto);

}
