package az.ingress.service;

import az.ingress.dto.JwtAuthResponse;
import az.ingress.dto.JwtRefreshTokenRequest;
import az.ingress.dto.LoginDto;
import jakarta.servlet.http.HttpServletRequest;

public interface AuthenticationService {
    JwtAuthResponse login(LoginDto loginDto);

    JwtAuthResponse refreshToken(HttpServletRequest request);


}
