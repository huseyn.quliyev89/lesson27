package az.ingress.exception;

public class CustomJwtException extends RuntimeException{
    public CustomJwtException(String msg) {
        super(msg);
    }
}
